# frozen_string_literal: true

module Ical
  class Engine < ::Rails::Engine
    isolate_namespace Ical
    config.generators.api_only = true
  end
end
