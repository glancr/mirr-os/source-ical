# frozen_string_literal: true

module Ical
  require 'icalendar'
  require 'icalendar/recurrence'

  class Fetcher
    def initialize(url, options = {})
      @cal_map = {}
      @url = url
      @options = options
    end

    def calendars
      fetch
      options = []
      @cal_map.each_pair do |uid, cal|
        options << [uid, calendar_name(cal)]
      end
      options
    end

    def calendar_meta(calendar_uid)
      fetch
      cal = @cal_map[calendar_uid]
      {
        name: calendar_name(cal),
        description: cal&.x_wr_caldesc&.first&.force_encoding(Encoding::UTF_8),
        color: cal&.x_apple_calendar_color&.first&.force_encoding(Encoding::UTF_8)
      }
    end

    def events(calendar_uid)
      fetch
      subset = select_events(calendar_uid)
      event_hashes = subset.map do |event|
        # FIXME: Icalendar::Recurrence currently does not support different summary/location for recurrences, so we set the base data once.
        base_data = map_event_base(event)

        if event.rrule.empty?
          base_data.merge(
            uid: event.uid.value,
            dtstart: event.dtstart,
            dtend: event.dtend,
            several_days: several_days?(event)
          )
        else
          today = Time.zone.today
          now = Time.zone.now
          occurrence_arr = []
          # FIXME: We already looped over occurrences in select_events. Maybe mutate the occurrences array there and just map it here.
          event.occurrences_between(
            today, today + 30
          ).each.with_index do |occurrence, i|
            next if occurrence.end_time < now

            occurrence_arr << base_data.merge(
              uid: "#{event.uid}-occurrence-#{i}",
              dtstart: occurrence.start_time,
              dtend: occurrence.end_time,
              several_days: several_days?(occurrence)
            )
          end
          occurrence_arr.flatten
          # FIXME: If an occurrence has different details, icalendar parses it
          # as a single event. Map to occurence via https://icalendar.org/iCalendar-RFC-5545/3-8-4-4-recurrence-id.html
        end
      end
      event_hashes.flatten
    end

    def reminders(list_uid)
      fetch
      @cal_map[list_uid].todos.map do |reminder|
        # TODO: Add support for recurring todos
        {
          uid: reminder.uid&.value,
          due_date: reminder.due,
          creation_date: reminder.created,
          completed: reminder.status.eql?('COMPLETED'),
          summary: reminder.summary&.value&.force_encoding(Encoding::UTF_8),
          description: reminder.description&.value&.force_encoding(Encoding::UTF_8)
        }
      end
    end

    private

    def fetch
      return unless @cal_map.empty?

      content = HTTParty.get(@url, @options)
      Icalendar::Parser.new(content).parse.map.with_index do |cal, i|
        uid = generate_uid(@url, i)
        raise StandardError, "[ical] Could not generate UID from URL #{url} and index #{i}" if uid.nil?

        @cal_map[uid] = cal
      end
    end

    def select_events(calendar_uid)
      now = Time.now.utc
      today = Time.zone.today
      in_30_days = today + 30

      @cal_map[calendar_uid].events.select do |event|
        reference = event.dtend.nil? ? event.dtstart : event.dtend
        # Ensures that all-day events that have started today are included
        range = reference.instance_of?(Icalendar::Values::Date) ? [today, in_30_days] : [now, in_30_days]
        reference.between?(*range) || event.occurrences_between(*range).length.positive?
      end
    end

    def generate_uid(url, index)
      "#{Digest::SHA1.hexdigest(url)}_#{index}"
    end

    def calendar_name(cal)
      return 'no title' if cal.nil?

      (cal.x_wr_calname.first&.value || cal.name).force_encoding(Encoding::UTF_8)
    end

    # Pulls attributes required by mirr.OS' event schema from an iCal event object.
    # @param [Icalendar::Event] event the event to map to our schema.
    # @return [Hash] Event hash with the CalendarEvent schema properties.
    def map_event_base(event)
      {
        all_day: event.dtstart.is_a?(Icalendar::Values::Date),
        summary: event.summary&.force_encoding(Encoding::UTF_8),
        description: event.description&.force_encoding(Encoding::UTF_8),
        location: event.location&.force_encoding(Encoding::UTF_8)
      }
    end

    # Checks whether an event spans several days.
    # Returns false if an event has no end date, see spec: https://tools.ietf.org/html/rfc5545#section-3.6.1
    # @param [Icalendar::Event|Icalendar::Recurrence::Occurrence] event Either an event with dtstart and dtend properties, or a occurrence object with start_time and end_time.
    # @return [Boolean]
    def several_days?(event)
      case event
      when Icalendar::Event
        event.dtend.present? && ((event.dtend.to_datetime - event.dtstart.to_datetime) <=> 1).positive?
      when Icalendar::Recurrence::Occurrence
        event.end_time.present? && ((event.end_time.to_datetime - event.start_time.to_datetime) <=> 1).positive?
      else
        false
      end
    end

  end
end
